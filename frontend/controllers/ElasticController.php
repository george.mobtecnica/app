<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\Elastic;

class ElasticController extends Controller
{

    public function actionIndex()
    {

        $elastic        = new Elastic();
        $elastic->name  = 'ahmed';
        $elastic->email = 'ahmedkhan_847@hotmail.com';
        if ($elastic->insert()) {
            echo "Added Successfully";
        } else {
            echo "Error";
        }

    }

}