<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_session".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $session_id
 * @property string $create_date
 * @property string $expiry_date
 */
class UserSession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'number'],
            [['session_id', 'create_date', 'expiry_date'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'session_id' => 'Session ID',
            'create_date' => 'Create Date',
            'expiry_date' => 'Expiry Date',
        ];
    }
    public function generateSessionId() {

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $length = 10;
        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < $length; $i++) {

            $str .= $chars[rand(0, $size - 1)];
        }

        $this->session_id = $str;
    }
}
