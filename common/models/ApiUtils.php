<?php

namespace common\models;

use common\models\User;
use common\models\UserSearch;

class ApiUtils {

    public static function setHeader($status) {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . self::_getStatusCodeMessage($status);
        $content_type = "application/json; charset=utf-8";

        header($status_header);
        header('Content-type: ' . $content_type);
        header('X-Powered-By: ' . "mobtecnica.com");
    }

    public static function _getStatusCodeMessage($status) {
        $codes = Array(
            201 => 'Created',
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public static function createResponse($code, $status, $data = null, $message = null, $errors = null) {
        $response = '';
        $response['status'] = $status;
        $response['data'] = [];
        $response ['message'] = '';
        $response ['errors'] = '';
        if ($data) {
            if (is_array($data)) {
                $response ['total-items'] = sizeof($data);
            }
            $response ['data'] = $data;
        }
        if ($message) {
            $response ['message'] = $message;
        }
        if ($errors) {
            $response ['errors'] = $errors;
        }
        self::setHeader($code);
        \Yii::$app->response->format = 'json';

        if ($code != 200) {
            echo json_encode($response);
            exit;
        }
        return $response;
    }

    public static function validateSession() {
        
        if (!isset($_POST['session_id'])) {
            return self::createResponse(400, 0, null, 'session_id required');
        }

        $id         = $_POST['id'];
        $session_id = $_POST['session_id'];
        $date       = date("Y-m-d G:i:s");
        $session = UserSession::find()->where("user_id = $id and session_id = '$session_id' and expiry_date > '$date'  ")->one();
        if (!$session) {
            return self::createResponse(403, 0, null, 'Session Not valid');
        }
    }

    public static function validateUser() {

        if (!isset($_POST['id'])) {
            return self::createResponse(400, 0, null, 'user id required');
        }
        $user = User::findOne(['id' => $_POST['id']]);
        if (!$user) {
            return self::createResponse(400, 0, null, 'User not valid ');
        }
/*
        if ($user->user_type_id != User::CUSTOMER) {
            return self::createResponse(400, 0, null, 'User not valid.');
        }
*/
        return $user;
    }
	public static function validateEmail() {
	echo $_POST['email'];
        if (!isset($_POST['email'])) {
            return self::createResponse(400, 0, null, 'Email required');
        }
       // $user = Users::findOne(['email' => $_POST['email']]);
       // if (!$user) {
       //     return self::createResponse(400, 0, null, 'Email Exists');
      //  }

       // return $user;
    }

}
