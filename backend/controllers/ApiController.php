<?php

namespace backend\controllers;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use common\models\ApiUtils;
use common\models\UserSession;
use common\models\User;
use common\models\Products;
use common\models\UserSearch;

//use app\models\ImageUpload;
use yii\web\UploadedFile;
use yii\db\Query;

class ApiController extends Controller {

    private $validationExcluded = ['login', 'signup', 'test'];
    private $user = null;

    public function beforeAction($action) {

        $this->enableCsrfValidation = false;
        if (!in_array($action->id, $this->validationExcluded)) {
            $this->user = ApiUtils::validateUser();
            ApiUtils::validateSession();
        }
        \Yii::$app->response->format = 'json';

        return parent::beforeAction($action);
    }

    public function actionTest() {

        return ApiUtils::createResponse(200, 1, ['Test' => 'test value'], 'test-success');
     
     }

    
    //SignUp

    public function actionSignup(){
       if (isset($_POST['firstname']) && isset($_POST['password']) && isset($_POST['email'])) {


        $emails = User::findOne(['email' => $_POST['email']]);

            $model = new User();

        if($emails){
            return ApiUtils::createResponse(200, 0, null, 'Email Exists');
        }

            $model->firstname           = $_POST['firstname'];
            $model->lastname            = $_POST['lastname'];
            $model->email               = $_POST['email'];
            $model->password            = sha1($_POST['password']);
            $model->user_type           = 2;

            if(isset($_POST['phone'])): 
                        $model->phone = $_POST['phone'];
            endif;
             $model->status         = 1;
            if($model->validate()){
               $model->save();
               return ApiUtils::createResponse(201, 1, $model, 'User added sucessfully');  
            }else{
               return ApiUtils::createResponse(200, 0, null, 'Signup Failed');  
            }
            
        }else{
            return ApiUtils::createResponse(400, 0, null, 'email and password is required');
        }
    }

   
    //User Login


    public function actionLogin() {
        if (isset($_POST['email']) && isset($_POST['password'])) {

            $model = User::findOne(['email' => $_POST['email'],
                        'password' => sha1($_POST['password'])]);

            if ($model) {
                $session                            = new UserSession();
                $session->generateSessionId();
                $session->user_id                   = $model->id;
                $session->create_date               = date("Y-m-d G:i:s");
                $session->expiry_date               = date('Y-m-d', strtotime("+ 30" . " day"));
                if ($session->save()) {
                    return ApiUtils::createResponse(200, 1, ['session_id' => $session->session_id,
                                'user' => ['id' => $model->id]], 'Success');
                }
            } 
            else {
                return ApiUtils::createResponse(200, 0, null, ' Incorrect username or password');
            }
        } 
        else {
            return ApiUtils::createResponse(400, 0, null, 'username and password is required');
        }
    }


    //Add product

    public function actionAddproduct(){
        if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['price'])) {

            $model = new Products();
            $model->title               = $_POST['title'];
            $model->description         = $_POST['description'];
            $model->price               = $_POST['price'];
            if($model->validate()){
               $model->save();
               return ApiUtils::createResponse(201, 1, $model, 'Product added sucessfully');  
            }else{
               return ApiUtils::createResponse(200, 0, null, 'Product addeding failed');  
            }
        }else{
            return ApiUtils::createResponse(400, 0, null, 'data is required');
        }
    }


    //Get product

    public function actionGetproduct(){

        if (isset($_POST['product_id']) ) {

            $model = Products::findOne( $_POST['product_id']);
            if(!empty($model)){
                return ApiUtils::createResponse(201, 1, [ "id" =>$model->id,
                    "title" =>$model->title,
                    "description" =>$model->description,
                    "price" =>$model->price,
                     ], 'Success');  
            }else{
               return ApiUtils::createResponse(200, 0, null, 'No record found');  
            }
            
        }else{
            return ApiUtils::createResponse(400, 0, null, 'Prodict id required');
        }
    }




    //User Logout
    public function actionLogout() {
        $session = UserSession::find()->where('user_id = '
                        . $_POST['id'] . ' and session_id ="' . $_POST['session_id'] . '"')->one();
        if ($session && $session->delete()) {

            return ApiUtils::createResponse(200, 0, null, 'Logged out successfully');
        }
        return ApiUtils::createResponse(403, 0, null, 'Logged out failed');
    }
    
    


} 


     
            